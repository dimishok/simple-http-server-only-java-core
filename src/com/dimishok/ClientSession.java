package com.dimishok;



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Date;


public class ClientSession implements Runnable {

    private Socket socket;
    private InputStream in = null;
    private OutputStream out = null;
    private static final String DEFAULT_FILES_DIR = "/www";

    @Override
    public void run() {
        try {
            String header = readHeader();
            System.out.println(header + "\n");
            String uri = getURIFromHeader(header);
            System.out.println("Resource: " + uri + "\n");
            int code = send(uri);
            System.out.println("Result code: " + code + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

     ClientSession(Socket socket) throws IOException {
        this.socket = socket;
        initialize();
    }

    private void initialize() throws IOException {

        in = socket.getInputStream();
        out = socket.getOutputStream();
    }


    private String readHeader() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder builder = new StringBuilder();
        String ln;
        while (true) {
            ln = reader.readLine();
            if (ln == null || ln.isEmpty()) {
                break;
            }
            builder.append(ln);
            builder.append(System.getProperty("line.separator"));
        }
        return builder.toString();
    }


    private String getURIFromHeader(String header) {
        int from = header.indexOf(" ") + 1;
        int to = header.indexOf(" ", from);
        String uri = header.substring(from, to);
        int paramIndex = uri.indexOf("?");
        if (paramIndex != -1) {
            uri = uri.substring(0, paramIndex);
        }
        return DEFAULT_FILES_DIR + uri;
    }


    private int send(String uri) throws IOException {
        InputStream strm = HttpServer.class.getResourceAsStream(uri);
        int code = (strm != null) ? 200 : 404;
        String contentType = (uri.endsWith("xml")) ? "text/xml" : "text/html";
        String header = getHeader(code, contentType);
        PrintStream answer = new PrintStream(out, true, "UTF-8");
        answer.print(header);
        if (code == 200) {
            int count = 0;
            byte[] buffer = new byte[1024];
            while((count = strm.read(buffer)) != -1) {
                out.write(buffer, 0, count);
            }
            strm.close();
        }
        return code;
    }

    private String getHeader(int code, String contentType) {
        StringBuilder buffer = new StringBuilder();
        buffer.append("HTTP/1.1 ");
        buffer.append(code);
        buffer.append(" ");
        buffer.append(getAnswer(code));
        buffer.append("\n");
        buffer.append("Date: ");
        buffer.append(new Date());
        buffer.append("\n");
        buffer.append("Accept-Ranges: none\n");
        buffer.append("Content-Type: ");
        buffer.append(contentType);
        buffer.append("\n");
        buffer.append("\n");
        return buffer.toString();
    }

    private String getAnswer(int code) {
        switch (code) {
            case 200:
                return "OK";
            case 404:
                return "Not Found";
            default:
                return "Internal Server Error";
        }
    }


}
